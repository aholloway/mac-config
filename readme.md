#Mac Config

Configuration of system and applications for a new computer.

## App Store Installs
* [Airmail](https://itunes.apple.com/gb/app/airmail/id573171375)
* [Caffeine](https://itunes.apple.com/gb/app/caffeine/id411246225)
* [CopyClip](https://itunes.apple.com/gb/app/copyclip-clipboard-history/id595191960)
* [Folder Magic](https://itunes.apple.com/gb/app/folder-magic/id647731944)
* [Memory Clean](https://itunes.apple.com/gb/app/memory-clean/id451444120)
* [Xcode](https://itunes.apple.com/gb/app/xcode/id497799835)

## External Installs
* [Alfred](http://www.alfredapp.com/)
* [Chrome](https://www.google.com/intl/en/chrome/browser/)
* [Dash](http://kapeli.com/dash)
* [FileZilla](https://filezilla-project.org/)
* [Firefox](http://www.mozilla.org/en-US/firefox/new/)
* [ImageOptim](http://imageoptim.com/)
* [iTerm](http://www.iterm2.com/)
* [LittleIpsum](http://littleipsum.com/)
* [MAMP](http://www.mamp.info/en/index.html)
* [Mou](http://25.io/mou/) / [MacDown](http://macdown.uranusjr.com/)
* [Node.js](http://nodejs.org/download/)
* [OpenOffice](https://www.openoffice.org/)
* [Pastor](http://download.cnet.com/Pastor/3000-18501_4-8539.html)
* [Robomongo](http://robomongo.org/)
* [Sequal Pro](http://www.sequelpro.com/)
* [SourceTree](http://www.sourcetreeapp.com/)
* [Spectacle](http://spectacleapp.com/)
* [Sublime Text](http://www.sublimetext.com/)
* [Todoist](https://en.todoist.com/)
* [TinkerTool](http://www.bresink.com/osx/TinkerTool.html)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

## Terminal

### Command Line Installs
* [Composer](https://getcomposer.org/download/)
* [Git](http://git-scm.com/book/en/Getting-Started-Installing-Git)
* [Homebrew](http://brew.sh/)
* [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)

### Gems
* [bundler](https://rubygems.org/gems/bundler)
* [rake](https://rubygems.org/gems/rake)
* [sass](https://rubygems.org/gems/sass)
* [scss-lint](https://rubygems.org/gems/scss-lint)

### Homebrew
* php54
* php54-mycrypt
* zsh
* tmux

### Node Packages
* [bower](http://bower.io/)
* [grunt-cli](http://gruntjs.com/getting-started)

### Zsh settings
```
# Path to oh-my-zsh installation
export ZSH=$HOME/.oh-my-zsh

# Theme
ZSH_THEME="robbyrussell"

# Plugins
plugins=(npm brew git vagrant bower)
source $ZSH/oh-my-zsh.sh

# Auto run tmux on startup
[[ $TERM != "screen" ]] && exec tmux
```

## Sublime Text

## Packages
* Alignment
* AutoFileName
* DocBlockr
* Emmet
* GitGutter
* Handlebars
* Sass
* SideBarEnhancements
* SublimeCodeIntel
* SublimeLinter
* Theme - Soda
* [Package Control](https://sublime.wbond.net/installation)


## Color Scheme
* [Monokai Soda](buymeasoda.github.com/soda-theme/extras/colour-schemes.zip)

### Settings - User

```
{
  "theme": "Soda Light.sublime-theme",
  "color_scheme": "Packages/Color Scheme - Default/Monokai Soda.tmTheme",
  "soda_classic_tabs": true,
  "soda_folder_icons": false,

  "font_size": 15,
  "highlight_line": true,
  "line_padding_top": 2,
  "line_padding_bottom": 2,

  "tab_size": 2,
  "translate_tabs_to_spaces": true,
  "highlight_modified_tabs": true,

  "word_wrap": true,
  "bold_folder_labels": true,
  "open_files_in_new_window": true
}
```

### Key Bindings - User

```
[
  { "keys": ["super+v"], "command": "paste_and_indent" },
  { "keys": ["super+shift+v"], "command": "paste" }
]
```

## Browser Add-ons
* ColorZilla
* Emmet Re:View
* Firebug
* FireSass
* Flagfox
* Google Analytics Debugger
* LastPass
* MeasureIt
* Pesticide (CSS)
* Pixlr Grabber
* Pocket
* Web Developer
* PerfMap
